'use strict'

function callbackHell (callback) {
  const test = 'Hola javascript!'
  callback(test)
}

module.exports = {
  callbackHell
}
