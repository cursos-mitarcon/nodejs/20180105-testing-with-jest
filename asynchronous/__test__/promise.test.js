'use strict'

import { getDataFromApi } from '../promise'

describe('Probando promesas', () => {
  test('Realizando petición a una api con async/await', async () => {
    // pagina original https://rickandmortyapi.com/api/character/
    // En el ejemplo uso una pagina que me permite crear url para pruebas http://www.mocky.io/v2/5c3a46582f00008200b5dc3a
    const url = 'https://rickandmortyapi.com/api/character/'
    const res = await getDataFromApi(url)
    expect(res.data.results.length).toBeGreaterThan(0)
    // done() //Con async y await ya no es necesario usar el done
  })

  test('Realizando petición a una api con promesa', async done => {
    // pagina original https://rickandmortyapi.com/api/character/
    // En el ejemplo uso una pagina que me permite crear url para pruebas http://www.mocky.io/v2/5c3a46582f00008200b5dc3a
    const url = 'https://rickandmortyapi.com/api/character/'
    getDataFromApi(url).then(({ data }) => {
      expect(data.results.length).toBeGreaterThan(0)
      done()
    })
  })

  test('Recibiendo un hola', () => {
    return expect(Promise.resolve('Hola')).resolves.toEqual('Hola')
  })

  test('Recibiendo un error', () => {
    return expect(Promise.reject(new Error('Mucho error'))).rejects.toThrowError('Mucho error')
  })

  test('Realizando llamada a un api con error', async () => {
    const apiError = 'http://httpstat.us/500'
    // Esta deberia ser la manera correcta para comparar el objeto interno y no desde el new Error
    // await expect(getDataFromApi(apiError)).rejects.toThrowError('500 Internal Server Error')
    expect(getDataFromApi(apiError)).rejects.toEqual(new Error('Request failed with status code 500'))
  })
})
