'use strict'

import { callbackHell } from '../callback'

describe('Probando llamadas asincronas', () => {
  test('Probando callback basico', done => {
    function otherCallback (data) {
      expect(data).toBe('Hola javascript!')
      done()
    }

    callbackHell(otherCallback)
  })
})
