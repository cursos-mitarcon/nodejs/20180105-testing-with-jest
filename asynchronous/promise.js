'use strict'

import axios from 'axios'

module.exports = {
  getDataFromApi: (url) => axios(url)
}
