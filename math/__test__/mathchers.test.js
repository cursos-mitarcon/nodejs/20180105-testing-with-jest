'use strict'

const user1 = { 'name': 'Caren', 'lastname': 'Rodriguez' }
const user2 = { 'name': 'Caren', 'lastname': 'Rodriguez' }
const user3 = { 'name': 'Daniela', 'lastname': 'Rodriguez' }

describe('Prueba de equals', () => {
  test('Son iguales', () => {
    expect(user1).toEqual(user2)
  })

  test('Son diferentes', () => {
    expect(user1).not.toEqual(user3)
  })
})
