'use strict'

import { add, multiply, divide, subtract } from '../math.js'

describe('Calculos matematicos', () => {
  test('Prueba de sumas', () => {
    expect(add(1, 1)).toBe(2)
  })

  test('Prueba de multiplciar', () => {
    expect(multiply(2, 1)).toBe(2)
  })

  test('Prueba de dividir', () => {
    expect(divide(6, 3)).toBe(2)
  })

  test('Prueba de restar', () => {
    expect(subtract(3, 1)).toBe(2)
  })
})
