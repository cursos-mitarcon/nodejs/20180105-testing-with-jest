'use strict'

describe('Es hora de las intantaneas', () => {

  // Para correr
  // $ npm run jest -- --snapshot.test.js
  // Para actualizar snapshot
  // $ npm run jest -- --updateSnapshot
  // npm run jest -- -u snapshot.test.js
  test('Tenemos una excepsion dentro del codigo', () => {
      const user = {
          id: Math.floor(Math.random() * 20),
          name: "Mitarcons"
      }
      expect(user).toMatchSnapshot({
          id:  expect.any(Number)
      });
  });

});
