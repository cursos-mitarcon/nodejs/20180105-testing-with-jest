'use strict'

import { getFruits } from '../arrays'

describe('Comprobamos que exista el elemento', () => {
  test('Contiene una banana', () => {
    expect(getFruits()).toContain('banana')
  })
  test('No Contiene una banana', () => {
    expect(getFruits()).not.toContain('cambur')
  })
  test('Comprobar el tamaño de un arreglo', () => {
    expect(getFruits()).toHaveLength(4)
  })
})
