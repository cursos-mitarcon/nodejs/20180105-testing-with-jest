'use strict'

const fruits = [
  'banana',
  'pera',
  'manzana',
  'fresa'
]

const colors = [
  'azul',
  'amarillo',
  'verde',
  'rojo',
  'blanco'
]

module.exports = {
  getColors: () => colors,
  getFruits: () => fruits
}
