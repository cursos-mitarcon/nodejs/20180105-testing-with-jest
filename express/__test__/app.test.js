'use strict'

const supertest = require('supertest')
const app = require('../app')

describe('Prueba de servidor', () => {
  test('Respuesta del metodo get del directorio raiz', async () => {
    const res = await supertest(app).get('/')
    expect(res.statusCode).toBe(200)
  })
})
