'use strict'

import { isNull, isTrue, isFalse, isUndefined } from '../true'

describe('Comparando Resultados de datos basico', () => {
  test('Probar resultados nulos', () => {
    expect(isNull()).toBeNull()
  })
  test('Probar resultados true', () => {
    expect(isTrue()).toBeTruthy()
  })
  test('Probar resultados false', () => {
    expect(isFalse()).toBeFalsy()
  })
  test('Probar resultados undefined', () => {
    expect(isUndefined()).toBeUndefined()
  })
})
