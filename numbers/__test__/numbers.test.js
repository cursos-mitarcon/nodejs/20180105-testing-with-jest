'use strict'

import { numbers } from '../numbers'
// import { numbers } from '../numbers'
// const { numbers } = require('../numbers')

describe('Comparando numeros', () => {
  test('Mayor que', () => {
    expect(numbers(2, 2)).toBeGreaterThan(3)
  })
  test('Mayor o igual que', () => {
    expect(numbers(2, 2)).toBeGreaterThanOrEqual(4)
  })
  test('Menor que', () => {
    expect(numbers(2, 2)).toBeLessThan(5)
  })
  test('Menor o igual que', () => {
    expect(numbers(2, 2)).toBeLessThanOrEqual(7)
  })
  test('Igual en numeros flotantes', () => {
    expect(numbers(0.2, 0.1)).toBeCloseTo(0.3)
  })
})
