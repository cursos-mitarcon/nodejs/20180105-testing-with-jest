Course of \"Curso de JavaScript Testing con Jest\" from platzi

math => Primera activdad del proyecto, ejemplo sencillo de Jest



Usamos babel-eslint
  * https://www.npmjs.com/package/babel-eslint
  * https://eslint.org/docs/rules/

Para formato de texto usamos standard
  npm run lint -- --fix => para corregir automaticamente

Para correr pruebas
  * Para correr todas las pruebas que se encuentran en la carpeta __test__
  $ npm [test, t, tst]
  * Para correr un archivo en especifico
    - $ npm run jest -- <nombre de archivo> // no es necesario colocar .test
  $ npm run jest mathchers
  * Para correr con coverage
    - $ npm jest -- --coverage
    - $ jest --coverage // Necesario tener jest de manera global
    - $ npm run test:coverage
  * Para correr con un watcher
    - npm jest -- --watch
    - npm run test:watch
