'use strict'

import { textOne } from '../string'

describe('Comprobar cadena de texto', () => {
  test('Debe contener el siguiente texto', () => {
    expect(textOne).toMatch(/bonito/)
  })
  test('No contiene el siguiente texto', () => {
    expect(textOne).not.toMatch(/conejo/)
  })
  test('Comprobar la longitud de un texto', () => {
    expect(textOne).toHaveLength(15)
  })
})
